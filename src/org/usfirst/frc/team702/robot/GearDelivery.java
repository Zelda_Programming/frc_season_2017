package org.usfirst.frc.team702.robot;

import java.util.TimerTask;

import org.usfirst.frc.team702.tools.DebouncedBoolean;

import com.ctre.CANTalon;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.Timer;

public class GearDelivery {

	public enum GearState
	{
		GEAR_PRESENT,
		GEAR_WAITING_TO_RAISE,
		GEAR_NOT_PRESENT,
		GEAR_WAITING_TO_LOWER,
		MANUAL_GEAR_UP,
		MANUAL_GEAR_DOWN,
		MANUAL_FORCE_DOWN
	}
	
	private double time_to_wait_after_detection = 1;
	
	private double servo_value_gear_up = 0.625;
	private double servo_value_gear_down = 0.330;
	private int current_limit = 6;
	private double motor_up_speed = .5;
	private double motor_down_speed = -.2;

	private CANTalon m_cantalon;
	
	private boolean m_run_autonomously = true;
	private GearState m_current_gear_state = GearState.GEAR_PRESENT;
	private GearState m_next_gear_state = GearState.GEAR_PRESENT;
	
	public DebouncedBoolean m_debounced_gear_present;
	
	/**********************************************************
	 *  Threading
	 **********************************************************/
    private java.util.Timer m_scheduler;
    private double m_period = 0.05;
    private int m_times_in_state = 0;
    
    private boolean m_gear_upright = false;
    private boolean m_reset_to_auto_after_removal = true;
    
	// Timer for the gear
	private Timer m_gear_timer;

	private Servo m_actuator;
	private AnalogInput m_gear_sensor;
	
	/**
	 * GearTask is the private scheduler within gear that 
	 * automatically runs the gear state machine.
	 */
	private class GearTask extends TimerTask {

        private GearDelivery m_gear;

        public GearTask(GearDelivery gear) {
          if (gear == null) {
            throw new NullPointerException("Pixy Instance was null");
          }
          this.m_gear = gear;
        }

        @Override
        public void run() {
        	m_gear.run();
        }
      }
	
	
	/**
	 * Constructor
	 * 
	 * @param CANTalon gearMotor
	 */
	public GearDelivery(Servo actuator, AnalogInput gearSensor) {
		m_times_in_state = 0;
		m_gear_timer = new Timer();
		m_gear_timer.start();
		
		m_actuator = actuator;
		m_gear_sensor = gearSensor;
		
		m_debounced_gear_present = new DebouncedBoolean(10);
		
		// Start the thread
		this.start(getPeriod());
	}

	/**
	 * Constructor
	 * 
	 * @param CANTalon gearMotor
	 */
	public GearDelivery(CANTalon actuator, AnalogInput gearSensor) {
		m_times_in_state = 0;
		m_gear_timer = new Timer();
		m_gear_timer.start();
		
		m_cantalon = actuator;
		m_gear_sensor = gearSensor;
		
		m_debounced_gear_present = new DebouncedBoolean(10);
		
		// Start the thread
		this.start(getPeriod());
	}
	
	
	
	/**
	 * Raises the gear and stops auto mode
	 */
	public synchronized void raiseGear()
	{
		m_next_gear_state = GearState.MANUAL_GEAR_UP;
		m_current_gear_state = GearState.MANUAL_GEAR_UP;
	}
	
	/**
	 * Hold the gear down
	 */
	public synchronized void holdGearDown()
	{
		m_next_gear_state = GearState.MANUAL_FORCE_DOWN;
		m_current_gear_state = GearState.MANUAL_FORCE_DOWN;
	}
	
	/**
	 * Lowers the gear and stops auto mode
	 */
	public synchronized void lowerGear()
	{
		m_next_gear_state = GearState.MANUAL_GEAR_DOWN;
		m_current_gear_state = GearState.MANUAL_GEAR_DOWN;
	}
	
	/**
	 * Periodic gear task 
	 */
	public void run()
	{
		m_debounced_gear_present.update(m_gear_sensor.getVoltage() < 3.0);
		
		switch(m_current_gear_state)
		{
			case GEAR_NOT_PRESENT:
				if(m_times_in_state == 0)
				{
					m_gear_timer.reset();
					m_gear_timer.start();
					m_cantalon.enable();
				} m_times_in_state++;
				
				// If the gear is present
				if(m_debounced_gear_present.get())
				{
					m_next_gear_state = GearState.GEAR_WAITING_TO_RAISE;
				}
				
				// Set the servo position for gear down
				if(m_actuator != null)
				{
					m_actuator.setPosition(servo_value_gear_down);
				} else if(m_cantalon != null) {
					//m_cantalon.setCurrentLimit(current_limit);
					m_cantalon.set(0);
					//m_cantalon.disable();
				}
				
				m_gear_upright = false;
				
				break;
				
			case GEAR_WAITING_TO_RAISE:
				if(m_times_in_state == 0)
				{
					m_gear_timer.reset();
					m_gear_timer.start();
					m_cantalon.enable();
				} m_times_in_state++;
				
				if(m_debounced_gear_present.get() && m_gear_timer.get() > time_to_wait_after_detection)
				{
					m_next_gear_state = GearState.GEAR_PRESENT;
				} else if(!m_debounced_gear_present.get()) {
					m_next_gear_state = GearState.GEAR_NOT_PRESENT;
				}
				
				m_gear_upright = false;
				
				break;
				
			case GEAR_PRESENT:
				if(m_times_in_state == 0)
				{
					m_cantalon.enable();
					m_cantalon.setCurrentLimit(current_limit);
				} m_times_in_state++;
				
				if(!m_debounced_gear_present.get())
				{
					m_next_gear_state = GearState.GEAR_WAITING_TO_LOWER;
				}
				
				// Set the servo value for the gear up position
				// TODO: Make sure we can keep setting it
				// Set the position to gear up
				if(m_actuator != null)
				{
					m_actuator.setPosition(servo_value_gear_up);
				} else if(m_cantalon != null) {
					
					m_cantalon.set(motor_up_speed);
				}
				
				m_gear_upright = true;
				
				break;
				
			case GEAR_WAITING_TO_LOWER:
				if(m_times_in_state == 0)
				{
					m_cantalon.enable();
					m_cantalon.setCurrentLimit(current_limit);
					m_gear_timer.reset();
					m_gear_timer.start();
				} m_times_in_state++;
				
				
				if(m_cantalon != null) {
					m_cantalon.setCurrentLimit(current_limit);
					m_cantalon.set(motor_up_speed);
					
				}
				
				if(!m_debounced_gear_present.get() && m_gear_timer.get() > time_to_wait_after_detection)
				{
					m_next_gear_state = GearState.GEAR_NOT_PRESENT;
					
				} else if(m_debounced_gear_present.get()) {
					m_next_gear_state = GearState.GEAR_PRESENT;
				}
				
				m_gear_upright = true;
				
				break;
			case MANUAL_GEAR_DOWN:
				if(m_times_in_state == 0)
				{
					m_cantalon.enable();
					m_gear_timer.reset();
					m_gear_timer.start();
					
				} m_times_in_state++;
				
				// Set the position to gear down
				if(m_actuator != null)
				{
					m_actuator.setPosition(servo_value_gear_down);
				} else if(m_cantalon != null) {
					m_cantalon.set(0);
					//m_cantalon.setCurrentLimit(current_limit);
					//m_cantalon.set(motor_down_speed);
				}
				
				m_gear_upright = false;
				
				if(getAutonomous())
				{
					if(!m_debounced_gear_present.get())
					{
						m_next_gear_state = GearState.GEAR_NOT_PRESENT;
					} else {
						m_next_gear_state = GearState.GEAR_PRESENT;
					}
				}
				
				break;
			case MANUAL_GEAR_UP:
				if(m_times_in_state == 0)
				{
					m_cantalon.enable();
					m_cantalon.setCurrentLimit(current_limit);
				} m_times_in_state++;
				
				// Set the position to gear up
				if(m_actuator != null)
				{
					m_actuator.setPosition(servo_value_gear_up);
				} else if(m_cantalon != null) {
					m_cantalon.setCurrentLimit(current_limit);
					m_cantalon.set(motor_up_speed);
				}
				
				m_gear_upright = true;
				
				if(getAutonomous())
				{
					if(!m_debounced_gear_present.get())
					{
						m_next_gear_state = GearState.GEAR_NOT_PRESENT;
					} else {
						m_next_gear_state = GearState.GEAR_PRESENT;
					}
				}
				
				break;
				
			case MANUAL_FORCE_DOWN:
				if(m_times_in_state == 0)
				{
					m_cantalon.enable();
					m_cantalon.setCurrentLimit(current_limit);
				} m_times_in_state++;
				
				// Set the position to gear up
				if(m_actuator != null)
				{
					m_actuator.setPosition(servo_value_gear_down);
				} else if(m_cantalon != null) {
					m_cantalon.setCurrentLimit(current_limit);
					m_cantalon.set(-motor_up_speed);
				}
				
				m_gear_upright = false;
				
				if(getAutonomous())
				{
					if(!m_debounced_gear_present.get())
					{
						m_next_gear_state = GearState.GEAR_NOT_PRESENT;
					} else {
						m_next_gear_state = GearState.GEAR_PRESENT;
					}
				}
				
				break;
				
			default:
				m_next_gear_state = GearState.MANUAL_GEAR_UP;
				break;
		}
		
		m_current_gear_state = m_next_gear_state;
	}

	/**
     * Start a thread to run the gear
     * 
     * @param period - period in seconds to schedule update
     */
    private void start(double period)
    {
    	// Schedule the gear task to execute every <period> seconds
    	if(m_scheduler == null && period > 0)
    	{
    		this.setPeriod(period);
    		System.out.println("Attempting to enable gear at a " + Double.toString(period) + " second rate.");
    		m_scheduler = new java.util.Timer();
    		m_scheduler.schedule(new GearTask(this), 0L, (long) (this.getPeriod() * 1000));
    	} else {
    		System.out.println("Gear Thread already scheduled. Stop before starting a new thread.");
    	}
    }
    
    
    /**
     * Cancel a running timer and attempt to null the variable
     */
    public void stop()
    {
    	// If the timer object is not null, cancel the scheduler
    	if(m_scheduler != null)
    	{
    		System.out.println("Attempting to disable gear Thread");
    		m_scheduler.cancel();
    		
    		// Set the timer to NULL to allow it to be reallocated if necessary
			m_scheduler = null;
    		
    	} else {
    		// nothing to do
    	}
    }
    
	// Getters/setters
    /**
     * Sets the frequency the read process will occur at.
     * $
     * @param period in seconds
     */
    public synchronized void setPeriod(double period)
    {
    	if(period < 0)
    	{
    		throw new IllegalArgumentException("Period must be a positive value");
    	}
    	
    	m_period = period;
    }
    
    /**
     * Gets the frequency the read process will occur at.
     * $
     * @return period in seconds
     */
    public synchronized double getPeriod()
    {
    	return this.m_period;
    }
    
    /**
	 * @return the current gear state
	 */
	public synchronized GearState getCurrentGearState() {
		return m_current_gear_state;
	}


	public synchronized  boolean getAutonomous() {
		return m_run_autonomously;
	}


	public synchronized void setAutonomous(boolean m_run_autonomously) {
		this.m_run_autonomously = m_run_autonomously;
	}


	public synchronized boolean isGearUpright() {
		return m_gear_upright;
	}


	public boolean getResetToAutoAfterRemoval() {
		return m_reset_to_auto_after_removal;
	}


	public void setisResetToAutoAfterRemoval(boolean m_reset_to_auto_after_removal) {
		this.m_reset_to_auto_after_removal = m_reset_to_auto_after_removal;
	}


	/**
	 * @return the time_to_wait_after_detection
	 */
	public synchronized double getTimeToWaitAfterDetection() {
		return time_to_wait_after_detection;
	}


	/**
	 * @param time_to_wait_after_detection the time_to_wait_after_detection to set
	 */
	public synchronized void setTimeToWaitAfterDetection(double time_to_wait_after_detection) {
		this.time_to_wait_after_detection = time_to_wait_after_detection;
	}


	/**
	 * @return the servo_value_gear_up
	 */
	public synchronized double getServoValueGearUp() {
		return servo_value_gear_up;
	}


	/**
	 * @param servo_value_gear_up the servo_value_gear_up to set
	 */
	public synchronized void setServoValueGearUp(double servo_value_gear_up) {
		this.servo_value_gear_up = servo_value_gear_up;
	}


	/**
	 * @return the servo_value_gear_down
	 */
	public synchronized double getServoValueGearDown() {
		return servo_value_gear_down;
	}


	/**
	 * @param servo_value_gear_down the servo_value_gear_down to set
	 */
	public synchronized void setServoValueGearDown(double servo_value_gear_down) {
		this.servo_value_gear_down = servo_value_gear_down;
	}

	public int getCurrentLimit() {
		return current_limit;
	}

	public void setCurrentLimit(int current_limit) {
		this.current_limit = current_limit;
	}
	
	/**
	 * @return the motor_speed
	 */
	public synchronized double getMotorSpeed() {
		return motor_up_speed;
	}

	/**
	 * @param motor_speed the motor_speed to set
	 */
	public synchronized void setMotorSpeed(double motor_speed) {
		this.motor_up_speed = motor_speed;
	}

	/**
	 * @return the motor_down_speed
	 */
	public synchronized double getMotorDownSpeed() {
		return motor_down_speed;
	}

	/**
	 * @param motor_down_speed the motor_down_speed to set
	 */
	public synchronized void setMotorDownSpeed(double motor_down_speed) {
		this.motor_down_speed = motor_down_speed;
	}
 
}
