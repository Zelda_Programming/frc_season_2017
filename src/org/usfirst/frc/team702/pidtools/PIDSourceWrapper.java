package org.usfirst.frc.team702.pidtools;

import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;

public abstract class PIDSourceWrapper implements PIDSource{

	protected Object m_sensor;
	protected PIDSourceType m_pid_source_type = PIDSourceType.kDisplacement;
	public double default_value = 0;
	
	public PIDSourceWrapper(Object sensor)
	{
		m_sensor = sensor;
	}
	
	@Override
	public void setPIDSourceType(PIDSourceType pidSource) {
    	m_pid_source_type = pidSource;		
	}

	@Override
	public PIDSourceType getPIDSourceType() {
		return m_pid_source_type;
	}

	@Override
	public abstract double pidGet();

}
