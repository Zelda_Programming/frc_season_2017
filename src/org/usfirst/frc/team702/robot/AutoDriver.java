package org.usfirst.frc.team702.robot;

import java.util.TimerTask;

import org.usfirst.frc.team702.pidtools.AnglePIDController;
import org.usfirst.frc.team702.pidtools.PIDConstants;
import org.usfirst.frc.team702.pidtools.PIDOutputTarget;
import org.usfirst.frc.team702.pidtools.PIDSourceWrapper;
import org.usfirst.frc.team702.sensors.LidarLite;
import org.usfirst.frc.team702.sensors.LidarLitePWM;
import org.usfirst.frc.team702.sensors.PixyCmu5;
import org.usfirst.frc.team702.sensors.Ultrasonic;


import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * @author sbaron
 *
 */
public class AutoDriver {

	public enum StrafeOperation{
		IDLE,
		STRAFE
	}
	
	public enum RotateOperation{
		IDLE,
		TURNTOANGLE,
		TURNTOTARGET,
		ROTATEATRATE
	}
	
	public enum DriveOperation{
		IDLE,
		DRIVE,
		WIGGLE
	}
	
	public enum SensorDirection{
		FRONT,
		REAR
	}
	
	// Robot drive
	private RobotDrive 	m_drive;

	// Status Variables
	private boolean m_is_running = false;
	
	// PID Controllers
	public AnglePIDController turnPIDLoop;
	public PIDController   	strafePIDLoop;
	public PIDController   	drivePIDLoop;
	public AnglePIDController rotatePIDLoop;
	
	// PID Output Targets
	public PIDOutputTarget 	turnCommand;
	public PIDOutputTarget 	strafeCommand;
	public PIDOutputTarget 	driveCommand;
	public PIDOutputTarget 	rotateCommand;
	
	// Default PID Gains
	PIDConstants turnGain;
	PIDConstants strafeGain;
	PIDConstants driveGain;
	PIDConstants rotateGain;
	
	// PID Source Wrappers
	private PIDSourceWrapper turnSource;
	private PIDSourceWrapper strafeSource;
	private PIDSourceWrapper driveSource;
	private PIDSourceWrapper rotateSource;
	private AHRS m_imu;
	
	// State machine variables
	private RotateOperation m_current_rotate_operation = RotateOperation.IDLE;
	private RotateOperation m_next_rotate_operation = RotateOperation.IDLE;
	
	private DriveOperation m_current_drive_operation = DriveOperation.IDLE;
	private DriveOperation m_next_drive_operation = DriveOperation.IDLE;
	
	private StrafeOperation m_current_strafe_operation = StrafeOperation.IDLE;
	private StrafeOperation m_next_strafe_operation = StrafeOperation.IDLE;
	
	private boolean m_drive_invert = false;
	private boolean m_drive_when_centered = false;
	
	public static final double m_front_two_tgt_area_constant = 4675;
	public static final double m_front_one_tgt_area_constant = 2175;
	public static final double m_rear_two_tgt_area_constant = 4675; // TODO: Calibrate this
	public static final double m_rear_one_tgt_area_constant = 2175; // TODO: Calibrate this
	
	// Thread for AutoDriver to run
	private java.util.Timer m_scheduler;
	private double m_period = 0.02;
	
	private boolean stopDriveOnTarget  = false; // TODO: Decide if this is needed
	private boolean stopTurnOnTarget   = false; // TODO: Decide if this is needed
	private boolean stopStrafeOnTarget = false; // TODO: Decide if this is needed
	private boolean stopRotateOnTarget = true;
	private double wiggleValue = 0;
	
	/**
	 * PixyTask is the private scheduler within PixyCMU5 that 
	 * automatically performs I2C reads to get the frame data
	 * from the connected Pixy device.
	 */
	private class AutoDriveTask extends TimerTask {

        private AutoDriver m_auto_drive;

        public AutoDriveTask(AutoDriver drive) {
          if (drive == null) {
            throw new NullPointerException("AutoDrive Instance was null");
          }
          this.m_auto_drive = drive;
        }

        @Override
        public void run() {
        	m_auto_drive.run();
        }
      }
	
	/**
	 * Constructor - Must call setImu after IMU is initalized
	 * @param RobotDrive
	 * @param default turn_source
	 * @param default strafe_source
	 * @param default drive_source
	 */
	public AutoDriver(RobotDrive drive, PIDSource turn_source, PIDSource strafe_source, PIDSource drive_source)
	{		
		turnGain 	= new PIDConstants(0.01, 0.0005, 0, 0.0);
		strafeGain 	= new PIDConstants(2.0, 0.005, 0, 0);
		driveGain 	= new PIDConstants(0.16, 0.0, 0.05, 0.0);
		rotateGain 	= new PIDConstants(0.02, 0.0015, 0.01, 0.0);
		m_drive = drive;
		
		turnCommand = new PIDOutputTarget();
		strafeCommand = new PIDOutputTarget();
		driveCommand = new PIDOutputTarget();
		rotateCommand = new PIDOutputTarget();
		
		setTurnSource(turn_source);
		setStrafeSource(strafe_source);
		setDriveSource(drive_source);
		
		SmartDashboard.putNumber("Turn Command", 0.0);
		SmartDashboard.putNumber("Strafe Command", 0.0);
		SmartDashboard.putNumber("Drive Command", 0.0);
		SmartDashboard.putNumber("Rotate Command", 0.0);

	}

	/**
	 * Updates the SmartDashboard with values from the controllers
	 */
	public void updateDashboard()
	{
		SmartDashboard.putNumber("Turn Command", turnCommand.getOutput());
		SmartDashboard.putNumber("Strafe Command", strafeCommand.getOutput());
		SmartDashboard.putNumber("Drive Command", driveCommand.getOutput());
		SmartDashboard.putNumber("Rotate Command", rotateCommand.getOutput());
		
		if(turnPIDLoop != null)
		{
			SmartDashboard.putBoolean("Turn On Target", turnPIDLoop.onTarget());
			SmartDashboard.putNumber("Turn Error", turnPIDLoop.getError());
		}
		
		if(strafePIDLoop != null)
		{
			SmartDashboard.putBoolean("Strafe On Target", strafePIDLoop.onTarget());
			SmartDashboard.putNumber("Strafe Error", strafePIDLoop.getError());
		}
		
		if(drivePIDLoop != null)
		{
			SmartDashboard.putBoolean("Drive On Target", drivePIDLoop.onTarget());
			SmartDashboard.putNumber("Drive Error", drivePIDLoop.getError());
		}
		
		if(rotatePIDLoop != null)
		{
			SmartDashboard.putBoolean("Rotate On Target", rotatePIDLoop.onTarget());
			SmartDashboard.putNumber("Rotate Error", rotatePIDLoop.getError());
		}		
	}
	
	/**
	 * Updates the smart dashboard with gain values for the given PID controller
	 * @param PIDController
	 * @param Name
	 */
	public void updateDashboardPID(PIDController controller, String name)
	{
		
		SmartDashboard.putNumber("PID " + name + " - P", controller.getP());
		SmartDashboard.putNumber("PID " + name + " - I", controller.getI());
		SmartDashboard.putNumber("PID " + name + " - D", controller.getD());
	}
	
	/**
	 * Update the PID constants with values from the dashboard for the given controller
	 * @param controller
	 * @param name: "Turn","Drive","Strafe",'Rotate"
	 */
	public void getFromDashboard(PIDController controller, String name)
	{
		controller.setPID(SmartDashboard.getNumber("PID " + name + " - P", 0.0), SmartDashboard.getNumber("PID " + name + " - I", 0.0), SmartDashboard.getNumber("PID " + name + " - D", 0.0));
		
		PIDConstants tmp = new PIDConstants(controller.getP(), controller.getI(), controller.getD(), 0);
		
		switch(name)
		{
		case "Turn":
			turnGain = tmp;
			break;
		case "Drive":
			driveGain = tmp;
			break;
		case "Strafe":
			strafeGain = tmp;
			break;
		case "Rotate":
			rotateGain = tmp;
			break;
		}		
	
	}
	
	/**
	 * @return true if the strafe controller is on target
	 */
	public boolean isStrafeOnTarget()
	{
		if(strafePIDLoop.isEnabled())
			return strafePIDLoop.onTarget();
		else
			return true;
	}
	
	/**
	 * @return true if the turn controller is on target
	 */
	public boolean isTurnOnTarget()
	{
		if(turnPIDLoop.isEnabled())
			return turnPIDLoop.onTarget();
		else
			return true;
	}
	
	/**
	 * @return true if the drive controller is on target
	 */
	public boolean isDriveOnTarget()
	{
		if(drivePIDLoop.isEnabled())
			return drivePIDLoop.onTarget();
		else
			return true;
	}
	
	/**
	 * @return true if the rotate controller is on target
	 */
	public boolean isRotateOnTarget()
	{
		if(rotatePIDLoop.isEnabled())
			return rotatePIDLoop.onTarget();
		else
			return true;
	}
	
	/**
	 * Initializes the turning PID Loop
	 *
	 * @param PIDSource
	 * @param PIDOutputTarget
	 */
	private synchronized void initTurnPIDLoop(PIDSource source, PIDOutputTarget target)
	{
		if(turnPIDLoop != null)
		{
			turnPIDLoop.reset();
			turnPIDLoop.free();
			turnPIDLoop = null;
		}
		
		turnPIDLoop = new AnglePIDController(turnGain.kP,turnGain.kI,turnGain.kD, source, target);
		turnPIDLoop.setInputRange(-180.0, 180.0);
		turnPIDLoop.setOutputRange(-0.5,0.5);
		turnPIDLoop.setAbsoluteTolerance(0.5f);
		turnPIDLoop.setToleranceBuffer(1);
		turnPIDLoop.setContinuous(true);
		
		turnPIDLoop.reset();

		updateDashboardPID(turnPIDLoop, "Turn");
		
		// Add the PID Controller to the Test-mode dashboard, allowing manual
        //LiveWindow.addActuator("DriveSystem", "TurnController", turnPIDLoop);
	}
	
	/**
	 * Initializes the strafe PID Loop
	 *
	 * @param PIDSource
	 * @param PIDOutputTarget
	 */
	private synchronized void initStrafePIDLoop(PIDSource source, PIDOutputTarget target)
	{
		if(strafePIDLoop != null)
		{
			strafePIDLoop.reset();
			strafePIDLoop.free();
			strafePIDLoop = null;
		}
		
		strafePIDLoop = new PIDController(strafeGain.kP,strafeGain.kI,strafeGain.kD, source, target);
		strafePIDLoop.setInputRange(-1,1);
		strafePIDLoop.setOutputRange(-1.0, 1.0);
		strafePIDLoop.setAbsoluteTolerance(0.04f);
		strafePIDLoop.setToleranceBuffer(1);
		strafePIDLoop.setContinuous(false);
		
		strafePIDLoop.reset();

		updateDashboardPID(strafePIDLoop, "Strafe");
		
		// Add the PID Controller to the Test-mode dashboard, allowing manual
        //LiveWindow.addActuator("DriveSystem", "StrafeController", strafePIDLoop);
	}
	
	/**
	 * Initializes the drive PID Loop
	 *
	 * @param PIDSource
	 * @param PIDOutputTarget
	 */
	private synchronized void initDrivePIDLoop(PIDSource source, PIDOutputTarget target)
	{
		if(drivePIDLoop != null)
		{
			drivePIDLoop.reset();
			drivePIDLoop.free();
			drivePIDLoop = null;
		}
		
		drivePIDLoop = new PIDController(driveGain.kP,driveGain.kI,driveGain.kD, source, target);
		drivePIDLoop.setInputRange(0,45);
		drivePIDLoop.setOutputRange(-1, 1);
		drivePIDLoop.setAbsoluteTolerance(0.3f);
		drivePIDLoop.setToleranceBuffer(1);
		drivePIDLoop.setContinuous(false);
		
		drivePIDLoop.reset();

		updateDashboardPID(drivePIDLoop, "Drive");

		// Add the PID Controller to the Test-mode dashboard, allowing manual
        //LiveWindow.addActuator("DriveSystem", "DriveController", drivePIDLoop);
	}
	
	/**
	 * Initializes the rotate PID Loop
	 *
	 * @param PIDSource
	 * @param PIDOutputTarget
	 */
	private synchronized void initRotatePIDLoop(PIDSource source, PIDOutputTarget target)
	{
		if(rotatePIDLoop != null)
		{
			rotatePIDLoop.reset();
			rotatePIDLoop.free();
			rotatePIDLoop = null;
		}
		
		rotatePIDLoop = new AnglePIDController(rotateGain.kP,rotateGain.kI,rotateGain.kD, source, target);
		rotatePIDLoop.setInputRange(-180.0, 180.0);
		rotatePIDLoop.setOutputRange(-0.5,0.5);
		rotatePIDLoop.setAbsoluteTolerance(0.5f);
		rotatePIDLoop.setToleranceBuffer(1);
		rotatePIDLoop.setContinuous(true);

		updateDashboardPID(rotatePIDLoop, "Rotate");
		
		// Add the PID Controller to the Test-mode dashboard, allowing manual
        //LiveWindow.addActuator("DriveSystem", "RotateController", rotatePIDLoop);
	}
	
	/**
     * Start a thread to run the auto driver to run through the state machine
     * 
     * @param period - period in seconds to schedule update
     */
    private synchronized void start(double period)
    {
    	setPeriod(period);
   	
    	// Schedule the AutoDriver task to execute every <period> seconds
    	if(m_scheduler == null && period > 0)
    	{
    		System.out.println("Attempting to enable AutoDriver at a " + Double.toString(period) + " second rate.");
    	} else if(period > 0) {
    		System.out.println("Rescheduling AutoDriver Thread at a " + Double.toString(period) + " second rate.");
    		m_scheduler.cancel();
    		m_scheduler = null;
    	} else {
    		System.out.println("Period must be greater than zero! Given " + Double.toString(period));
    		return;
    	}
    	    	
    	// Start the timer
    	m_scheduler = new java.util.Timer();
		m_scheduler.schedule(new AutoDriveTask(this), 0L, (long) (this.getPeriod() * 1000));
    }
	
    /**
     * Cancel a running timer and attempt to null the variable
     */
    private synchronized void stop()
    {
    	resetPIDLoops();
    	m_is_running = false;
    	
    	// If the timer object is not null, cancel the scheduler
    	if(m_scheduler != null)
    	{
    		System.out.println("Disabling AutoDrive function.");
    		m_scheduler.cancel();
    		
    		// Set the timer to NULL to allow it to be reallocated if necessary
    		m_scheduler = null;
    	}
    }
    
    /**
     * Drive for a specified distance
     * 
     * @param distance
     */
    public synchronized void driveToDistance(double setpoint, SensorDirection sensor_dir)
    {
    	if(driveSource == null)
    		throw new NullPointerException("Drive PID Source not initialized!");
    	
    	if(drivePIDLoop.isEnabled())
    		drivePIDLoop.reset();
    	
    	switch(sensor_dir)
    	{
    		case FRONT:
    			
    			/* * *
    			 * When driving using the front sensor, if it starts 10 feet from the target and the setpoint 
    			 * is 15 feet, the error is going to be setpoint - input = 5. We want to drive backwards 
    			 * which would be a negative value, therefore using the front sensor we want to invert
    			 * the gain value
    			 * */
    			m_drive_invert = true;    			
    			break;
			case REAR:
    			m_drive_invert = false;
				break;
    	}
    	
    	    	
    	System.out.println("Driving to distance: " + Double.toString(setpoint));
    	
    	// Assign the target angle (now absolute) to the angleSetpoint
		m_current_drive_operation = DriveOperation.DRIVE;
		drivePIDLoop.setSetpoint(setpoint);
		driveSource.default_value = setpoint;
		drivePIDLoop.enable();

		start(0.01);
    }
    
    /**
     * Drive while wiggling for a specified distance
     * 
     * @param distance
     */
    public synchronized void wiggleToDistance(double setpoint, SensorDirection sensor_dir, double wiggleValue_i)
    {
    	if(driveSource == null)
    		throw new NullPointerException("Drive PID Source not initialized!");
    	
    	if(drivePIDLoop.isEnabled())
    		drivePIDLoop.reset();
    	
    	wiggleValue = wiggleValue_i;
    	
    	switch(sensor_dir)
    	{
    		case FRONT:
    			
    			/* * *
    			 * When driving using the front sensor, if it starts 10 feet from the target and the setpoint 
    			 * is 15 feet, the error is going to be setpoint - input = 5. We want to drive backwards 
    			 * which would be a negative value, therefore using the front sensor we want to invert
    			 * the gain value
    			 * */
    			m_drive_invert = true;    			
    			break;
			case REAR:
    			m_drive_invert = false;
				break;
    	}
    	
    	    	
    	System.out.println("Driving to distance: " + Double.toString(setpoint));
    	
    	// Assign the target angle (now absolute) to the angleSetpoint
		m_current_drive_operation = DriveOperation.WIGGLE;
		drivePIDLoop.setSetpoint(setpoint);
		drivePIDLoop.enable();

		start(0.005);
    }
    
    /**
     * Take an initial reading from the configured sensor and use it as the controller setpoint
     * to maintain the distance
     * 
     * @param SensorDirection
     */
    public synchronized void maintainDistance(SensorDirection sensor_dir)
    {
    	if(driveSource == null)
    		throw new NullPointerException("Drive PID Source not initialized!");
    	
    	switch(sensor_dir)
    	{
    		case FRONT:
    			
    			/* * *
    			 * When driving using the front sensor, if it starts 10 feet from the target and the setpoint 
    			 * is 15 feet, the error is going to be setpoint - input = 5. We want to drive backwards 
    			 * which would be a negative value, therefore using the front sensor we want to invert
    			 * the gain value
    			 * */
    			m_drive_invert = true;    			
    			break;
			case REAR:
    			m_drive_invert = false;
				break;
    	}
    	
    	double distSetpt = driveSource.pidGet();    	
    	
    	System.out.println("Driving to distance: " + Double.toString(distSetpt));
    	
    	// Assign the target angle (now absolute) to the angleSetpoint
		m_current_drive_operation = DriveOperation.DRIVE;
		drivePIDLoop.setSetpoint(distSetpt);
		drivePIDLoop.enable();

		this.start(0.01);
    }
    
    /**
     * Turn to an angle absolute or relative to the current angle
     * 
     * @param targetAngle
     * @param relative to current angle
     */
    public synchronized void turnToAngle(double targetAngle, boolean relative) 
    {
    	if(m_imu == null)
    		throw new NullPointerException("Navx is not initialized!");
    	
    	// If the relative flag is set, add the targetAngle to the current angle
		if(relative)
			targetAngle = AutoDriver.normalizeAngle(m_imu.getAngle()-targetAngle);
		
		System.out.println("Turning to angle: " + Double.toString(targetAngle));
		m_current_rotate_operation = RotateOperation.TURNTOANGLE;
		
		// Configure the IMU to return rate
		setRotateSource(m_imu, PIDSourceType.kDisplacement);
		
		// Assign the target angle (now absolute) to the angleSetpoint		
		rotatePIDLoop.setSetpoint(targetAngle);
		rotatePIDLoop.enable();
		this.start(0.01);
    }
    
    /**
     * Rotate the robot at a designated rate
     * 
     * @param targetRate in deg/sec
     * @param Clockwise
     */
    public synchronized void rotateAtRate(double targetRate, boolean direction) 
    {
    	if(m_imu == null)
    		throw new NullPointerException("Navx is not initialized!");
    			
		System.out.println("Rotating " + (direction ? "CW" : "CCW") + " at " + Double.toString(targetRate) + "deg/sec");

		// Assign the target angle (now absolute) to the angleSetpoint
		m_current_rotate_operation = RotateOperation.ROTATEATRATE;
		
		// Configure the IMU to return rate
		setRotateSource(m_imu, PIDSourceType.kRate);
		
		// Set the rotate loop setpoint positive or negative based on direction
		rotatePIDLoop.setSetpoint(direction ? -targetRate : targetRate); // TODO: Check direction
		rotatePIDLoop.enable();
		this.start(0.01);
    }
    
    /**
     * Turn to point at the detected target
     */
    public synchronized void turnToTarget()
    {
    	if(turnSource == null)
    		throw new NullPointerException("Turn Source is not initialized!");
    	
    	if(!(turnSource instanceof PIDSourceWrapper))
    		throw new RuntimeException("TurnPID Source is not of type Advanced PID Source");
    	
    	System.out.println("Turning to target");
    	
		m_current_rotate_operation = RotateOperation.TURNTOTARGET;
		
		turnPIDLoop.setSetpoint(0);
		turnPIDLoop.enable();
		this.start(0.01);
    }
    
    /**
     * Strafe to line up with the detected target
     */
    public synchronized void strafeToTarget()
    {
    	if(strafeSource == null)
    		throw new NullPointerException("Strafe Source is not initialized!");

    	if(!(strafeSource instanceof PIDSourceWrapper))
    		throw new RuntimeException("Strafe PID Source is not of type Advanced PID Source");
    	
    	System.out.println("Strafing to target");

		// Assign the target angle (now absolute) to the angleSetpoint
		m_current_strafe_operation = StrafeOperation.STRAFE;
		
		strafePIDLoop.setSetpoint(0);
		strafePIDLoop.enable();
		this.start(0.01);
    }    
    
    /**
     * Strafe to line up with the detected target
     */
    public synchronized void lineUpOnTarget()
    {
    	if(strafeSource == null)
    		throw new NullPointerException("Strafe Source is not initialized!");

    	if(!(strafeSource instanceof PIDSourceWrapper))
    		throw new RuntimeException("Strafe PID Source is not of type Advanced PID Source");
    	
    	if(turnSource == null)
    		throw new NullPointerException("Turn Source is not initialized!");
    	
    	if(!(turnSource instanceof PIDSourceWrapper))
    		throw new RuntimeException("TurnPID Source is not of type Advanced PID Source");
    	
    	System.out.println("Lining Up on target");

		// Assign the target angle (now absolute) to the angleSetpoint
		m_current_rotate_operation = RotateOperation.TURNTOTARGET;
		m_current_strafe_operation = StrafeOperation.STRAFE;
		
		// Set setpoints to zero - On Target
		turnPIDLoop.setSetpoint(0);
		turnPIDLoop.enable();
		
		strafePIDLoop.setSetpoint(0);
		strafePIDLoop.enable();
		this.start(0.01);
    }   
    
    
    /**
     * Stop the drive and reset the PID Loops
     */
    public synchronized void stopDrive()
    {
    	// Reset the state variables to IDLE
    	resetStateMachines();
    	
    	driveCommand.pidWrite(0);
    	strafeCommand.pidWrite(0);
    	turnCommand.pidWrite(0);
    	rotateCommand.pidWrite(0);
    	
    	// Set the motors to zero to stop
    	if(m_drive != null)
    		m_drive.mecanumDrive_Cartesian(0, 0, 0, 0);
    	
    	// Reset the PID loops
    	resetPIDLoops();
    	
    	// Stop the thread
    	stop();
    }    
    
    
    /**
     * Resets the internal state variables
     */
    private synchronized void resetStateMachines()
    {
    	m_next_rotate_operation = RotateOperation.IDLE;
    	m_current_rotate_operation = RotateOperation.IDLE;
    	m_next_drive_operation = DriveOperation.IDLE;
    	m_current_drive_operation = DriveOperation.IDLE;
    	m_current_strafe_operation = StrafeOperation.IDLE;
    	m_next_strafe_operation = StrafeOperation.IDLE;
    }
    
    /**
     * Resets and stops the PID loops if they are allocated
     */
    private synchronized void resetPIDLoops()
    {
    	// Disable the controllers
    	if(turnPIDLoop != null)
    		turnPIDLoop.reset();
    	
		if(strafePIDLoop != null)
    		strafePIDLoop.reset();
		
		if(drivePIDLoop != null)
    		drivePIDLoop.reset();
		
		if(rotatePIDLoop != null)
    		rotatePIDLoop.reset();
    }
    
	/**
	 * The step function of the AutoDriver class
	 */
	public synchronized void run() {
		updateDashboard();
		
		if(turnPIDLoop != null)
			updateDashboardPID(turnPIDLoop, "Turn");
		
		if(strafePIDLoop != null)
			updateDashboardPID(strafePIDLoop, "Strafe");
		
		if(drivePIDLoop != null)
			updateDashboardPID(drivePIDLoop, "Drive");
		
		if(rotatePIDLoop != null)
			updateDashboardPID(rotatePIDLoop, "Rotate");
		
		m_is_running = true;
		
		double driveCommandValue = 0;
		double strafeCommandValue = 0;
		double turnCommandValue = 0;
		
		// Run the state machine for the strafe loop
		switch(m_current_strafe_operation)
		{
			case IDLE:
				// Do nothing
				break;
				
			case STRAFE:
				strafeCommandValue = this.strafeCommand.getOutput();
				
				// If its on target stop the loop
				if(stopStrafeOnTarget && strafePIDLoop.onTarget())
				{
					if(strafePIDLoop != null)
						strafePIDLoop.reset();
					
					m_next_strafe_operation = StrafeOperation.IDLE;
				} else {
					m_next_strafe_operation = StrafeOperation.STRAFE;
				}
				break;

			default:
				m_next_strafe_operation = StrafeOperation.IDLE;
				break;
		
		} m_current_strafe_operation = m_next_strafe_operation;
		
		// Run the state machine for the drive loop
		switch(m_current_drive_operation)
		{
			case IDLE:
				// Do nothing
				break;
			case DRIVE:
				driveCommandValue = m_drive_invert ? -driveCommand.getOutput() : driveCommand.getOutput();
				
				// If the drive when centered flag is set, only drive when it is on target, otherwise zero it
				if(getDriveWhenCentered())
					driveCommandValue = turnPIDLoop.onTarget() ? driveCommandValue : 0;
				
				// If its on target stop the loop
				if(stopDriveOnTarget && drivePIDLoop.onTarget())
				{
					if(drivePIDLoop != null)
						drivePIDLoop.reset();
					
					m_next_drive_operation = DriveOperation.IDLE;
				} else {
					m_next_drive_operation = DriveOperation.DRIVE;
				}

				break;

			case WIGGLE:
				driveCommandValue = m_drive_invert ? -driveCommand.getOutput() : driveCommand.getOutput();
				
				wiggleValue = -wiggleValue;
				
				turnCommandValue = wiggleValue;
				
				// If its on target stop the loop
				if(stopDriveOnTarget && drivePIDLoop.onTarget())
				{
					if(drivePIDLoop != null)
						drivePIDLoop.reset();
					
					m_next_drive_operation = DriveOperation.IDLE;
				} else {
					m_next_drive_operation = DriveOperation.DRIVE;
				}

				break;
			default:
				m_next_drive_operation = DriveOperation.IDLE;
				break;
		} m_current_drive_operation = m_next_drive_operation;
		
		// Run the state machine for the turn loop
		switch(m_current_rotate_operation)
		{
			case IDLE:
				// Do nothing
				break;
				
			case TURNTOTARGET:
				turnCommandValue = -this.turnCommand.getOutput();
				
				// If its on target stop the loop
				if(stopTurnOnTarget && turnPIDLoop.onTarget())
				{
					if(turnPIDLoop != null)
						turnPIDLoop.reset();
					
					m_next_rotate_operation = RotateOperation.IDLE;
				} else {
					m_next_rotate_operation = RotateOperation.TURNTOTARGET;
				}
				break;
				
			case TURNTOANGLE: // Turn to an angle, rotation measured by the IMU
				turnCommandValue = -this.rotateCommand.getOutput();				
				
				// If its on target stop the loop
				if(stopRotateOnTarget && rotatePIDLoop.onTarget())
				{
					if(rotatePIDLoop != null)
						rotatePIDLoop.reset();
					
					m_next_rotate_operation = RotateOperation.IDLE;
				} else {
					m_next_rotate_operation = RotateOperation.TURNTOANGLE;
				}
				
				break;
				
			case ROTATEATRATE: // Rotate at a specific rate, rotation measured by the IMU
				turnCommandValue = -this.rotateCommand.getOutput();				
				m_next_rotate_operation = RotateOperation.ROTATEATRATE;
				
				break;
				
			default:
				m_next_rotate_operation = RotateOperation.IDLE;
				break;
		} m_current_rotate_operation = m_next_rotate_operation;
		
		// Set the command to the motors
		m_drive.mecanumDrive_Cartesian(strafeCommandValue,driveCommandValue,turnCommandValue,0);
	}

	
	/**
	 * @return the stopOnTarget
	 */
	public synchronized boolean isStopOnTarget() {
		return stopDriveOnTarget;
	}

	/**
	 * @param stopOnTarget the stopOnTarget to set
	 */
	public synchronized void setStopOnTarget(boolean stopOnTarget) {
		this.stopDriveOnTarget = stopOnTarget;
	}
	
	/**
	 * @return the turnSource
	 */
	public synchronized PIDSource getTurnSource() {
		return turnSource;
	}

	/**
	 * Set the source for the turn PID loop
	 * @param source
	 */
	public synchronized void setTurnSource(PIDSource source) {
		if(!(source instanceof PixyCmu5))
    		throw new RuntimeException("Turn PID Source must be of type PixyCmu5");
		
		this.turnSource = null;
		
		this.turnSource = new PIDSourceWrapper(source) {
			@Override
			public double pidGet() {
				if(m_sensor instanceof PixyCmu5)
				{ 
					if(((PixyCmu5) m_sensor).getNumObjectsDetected() == 0)
					{
						return 0;
					} else { 
						return -((PixyCmu5) m_sensor).getTargetAngle();
					}
				} else {
					return 0;
				}
			}
		};
		
		// Init the PID loop
		initTurnPIDLoop(turnSource, turnCommand);
	}

	/**
	 * @return the strafe source
	 */
	public synchronized PIDSource getStrafeSource() {
		return strafeSource;
	}

	/**
	 * Set the PID Source for the strafe loop
	 * @param source
	 */
	public synchronized void setStrafeSource(PIDSource source) {
		if(!(source instanceof PixyCmu5))
    		throw new RuntimeException("Strafe PID Source must be of type PixyCmu5");
		
		this.strafeSource = null;
		
		this.strafeSource = new PIDSourceWrapper(source) {
			@Override
			public double pidGet() {
				if(m_sensor instanceof PixyCmu5)
				{
					if(((PixyCmu5) m_sensor).getNumObjectsDetected() < 2)
					{
						return 0.0;
					} else {					
						return ((PixyCmu5) m_sensor).getNormalizedOffset(2);
					}
				} else {
					return 0;
				}
			}
		};
		
    	initStrafePIDLoop(strafeSource, strafeCommand);
	}

	public synchronized PIDSource getDriveSource() {
		return driveSource;
	}

	public synchronized void setDriveSource(PIDSource source) {
		
		if(!(source instanceof LidarLite || source instanceof LidarLitePWM || source instanceof PixyCmu5 || source instanceof Ultrasonic))
    		throw new RuntimeException("Drive PID Source must be of type Ultrasonic or PixyCmu5");
		
		// Clear the current drive source if it exists
		this.driveSource = null;
		
		this.driveSource = new PIDSourceWrapper(source) {
			
			double distance = 0;
			
			@Override
			public double pidGet() {
				if(m_sensor instanceof LidarLite)
				{
					return ((LidarLite) m_sensor).getDistance();
					
				} else if(m_sensor instanceof LidarLitePWM) {
					return ((LidarLitePWM) m_sensor).getDistance();
					
				} else if(m_sensor instanceof Ultrasonic) {
					return ((Ultrasonic) m_sensor).getDistance();
					
				} else if(m_sensor instanceof PixyCmu5) {
					
					int nPixyTgt = ((PixyCmu5) m_sensor).getNumObjectsDetected();
					
					if(nPixyTgt == 2)
					{
						distance = PixyCmu5.getCalcDistance( ((PixyCmu5) m_sensor).getTotalArea(2), m_front_two_tgt_area_constant);
					} else if(nPixyTgt == 1) {
						distance = PixyCmu5.getCalcDistance( ((PixyCmu5) m_sensor).getTotalArea(1), m_front_one_tgt_area_constant);
					} else {
						distance = default_value; // Set the distance to the default to ignore it
					}
					
					return distance;
				} else {
					return 0;
				}
			}
		};
		
    	initDrivePIDLoop(driveSource, driveCommand);
	}
	
	/**
	 * @return the rotate source
	 */
	private PIDSource getRotateSource() {
		return rotateSource;
	}
	
	/**
	 * Set the source for the turn PID loop
	 * @param source
	 * @param PIDSourceType, kRate or kDisplacement
	 */
	private synchronized void setRotateSource(PIDSource source, PIDSourceType type) {
		if(!(source instanceof AHRS))
    		throw new RuntimeException("Rotate PID Source must be of type AHRS");
		
		this.rotateSource = null;
		
		this.rotateSource = new PIDSourceWrapper(source) {
			@Override
			public double pidGet() {
				if(m_sensor instanceof AHRS) {
					if(type == PIDSourceType.kRate)
						return ((AHRS) m_sensor).getRate();
					else
						return ((AHRS) m_sensor).getAngle();
				} else {
					return 0;
				}
			}
		};
		
		// Init the PID loop
		initRotatePIDLoop(rotateSource, rotateCommand);
	}
	
	/**
	 * @return the m_period
	 */
	public synchronized double getPeriod() {
		return m_period;
	}

	/**
	 * @param m_period the m_period to set
	 */
	public synchronized void setPeriod(double m_period) {
		this.m_period = m_period;
	}
	
	public synchronized boolean getDriveWhenCentered() {
		return m_drive_when_centered;
	}

	public synchronized void setDriveWhenCentered(boolean m_drive_when_centered) {
		this.m_drive_when_centered = m_drive_when_centered;
	}

	/**
	 * @return the isRunning
	 */
	public synchronized boolean isRunning() {
		return m_is_running;
	}

	/**
	 * @param isRunning the isRunning to set
	 */
	public synchronized void setRunning(boolean isRunning) {
		this.m_is_running = isRunning;
	}
	
	
	/**
	 * Assign an AHRS object to the class
	 * 
	 * @param imu
	 */
	public synchronized void setImu(AHRS imu)
	{
		if(m_imu == null && imu != null)
		{
			m_imu = imu;
		} else {
			throw new NullPointerException("IMU Not added successfully");
		}
		
		setRotateSource(m_imu, PIDSourceType.kDisplacement);
	}
	
	/**
	 * Normalizes an angle between -180 and 180 degrees
	 * 
	 * @param angle in degrees
	 * @return normalized angle
	 */
	public static double normalizeAngle(double angle)
	{
		// reduce the angle  
		angle =  angle % 360; 

		// force it to be the positive remainder, so that 0 <= angle < 360  
		angle = (angle + 360) % 360;  

		// force into the minimum absolute value residue class, so that -180 < angle <= 180  
		if (angle > 180)  
			angle -= 360;  

		return angle;
	}
	
}
