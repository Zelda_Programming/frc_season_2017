package org.usfirst.frc.team702.sensors;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;

public class Ultrasonic implements PIDSource{

	private AnalogInput m_analog_in;
	private double m_scale_factor = 0.2977;
	private PIDSourceType m_pid_source_type = PIDSourceType.kDisplacement;
	
	public Ultrasonic(AnalogInput analog_input, double scale_factor)
	{
		if(analog_input != null)
		{
			m_analog_in = analog_input;
		}
		
		m_scale_factor = scale_factor;
	}
	
	public double getDistance()
	{
		return m_analog_in.getVoltage() / m_scale_factor;
	}

	@Override
	public void setPIDSourceType(PIDSourceType pidSource) {
    	m_pid_source_type  = pidSource;		
	}

	@Override
	public PIDSourceType getPIDSourceType() {
		return m_pid_source_type;
	}

	@Override
	public double pidGet() {
		return getDistance();
	}
	
}
